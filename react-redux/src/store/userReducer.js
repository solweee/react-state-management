import { ADD_USER, REMOVE_USER } from "./constants";

const defaultState = {
  user: [],
};

export const userReducer = (state = defaultState, action) => {
  switch (action.type) {
    case ADD_USER:
      return {
        ...state,
        user: [...state.user, action.payload],
      };
    case REMOVE_USER:
      return {
        ...state,
        user: state.user.filter((user) => user !== action.payload),
      };
    default:
      return state;
  }
};
