import { SUBSCRIBE, UNSUBSCRIBE } from "./constants";

const defaultState = {
  // subscribed: false,
  subscribed: localStorage.getItem("subscribeCheck") === "true",
};

export const isSubscribedReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SUBSCRIBE:
      return {
        ...state,
        subscribed: true,
      };
    case UNSUBSCRIBE:
      return {
        ...state,
        subscribed: false,
      };
    default:
      return state;
  }
};
