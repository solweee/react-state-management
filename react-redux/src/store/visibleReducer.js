import {TOGGLE_SECTION, SET_COMMUNITY_DATA } from "./constants";

const defaultState = {
  communityData: [],
  sectionVisible: false,
};

export const visibleReduser = (state = defaultState, action) => {
  switch (action.type) {
    case SET_COMMUNITY_DATA:
      return {
        ...state,
        communityData: action.payload,
      };
    case TOGGLE_SECTION:
      return {
        ...state,
        sectionVisible: !state.sectionVisible,
      };
    default:
      return state;
  }
};
