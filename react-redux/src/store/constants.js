export const ADD_USER = "ADD_USER";
export const REMOVE_USER = "REMOVE_USER";

export const SUBSCRIBE = "SUBSCRIBE";
export const UNSUBSCRIBE = "UNSUBSCRIBE";

export const TOGGLE_SECTION = "TOGGLE_SECTION";
export const SET_COMMUNITY_DATA = "SET_COMMUNITY_DATA";
