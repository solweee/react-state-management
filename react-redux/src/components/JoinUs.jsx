import React, { useEffect, useState } from "react";
import { subscribe, unsubscribe } from "../api/api";
import { useDispatch, useSelector } from "react-redux";
import {
  addUserAction,
  isSubscribedAction,
  isUnsubscribedAction,
  removeUserAction,
} from "../store/actions";

const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com"];

function validate(email) {
  return VALID_EMAIL_ENDINGS.some((el) => email.endsWith(el));
}

export function JoinUs() {
  const dispatch = useDispatch();
  // const subscribedUser = useSelector((state) => state.users.user);  

  const [email, setEmail] = useState(localStorage.getItem("email") || "");
  // const [isSubscribed, setIsSubscribed] = useState(
  //   localStorage.getItem("subscribeCheck") === "true"
  // );
  const isSubscribed = useSelector((state) => state.subscribe.subscribed)
  const [isButtonDisabled, setIsButtonDisabled] = useState(false);
  const [buttonText, setButtonText] = useState(
    isSubscribed ? "Unsubscribe" : "Subscribe"
  );

  useEffect(() => {
    localStorage.setItem("email", email);
    localStorage.setItem("subscribeCheck", isSubscribed.toString());
  }, [email, isSubscribed]);

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleSubscribe = (e) => {
    e.preventDefault();
    const isValid = validate(email);
    if (!isValid) {
      // setIsSubscribed(false);
      dispatch(isUnsubscribedAction())
      setButtonText("Subscribe");
      return;
    }
    setIsButtonDisabled(true);

    if (!isSubscribed) {
      subscribe(email)
        .then(() => {
          // setIsSubscribed(true);
          dispatch(isSubscribedAction())
          setIsButtonDisabled(false);
          setButtonText("Unsubscribe");
          dispatch(addUserAction(email));
        })
        .catch((err) => {
          console.log(err, err.data);
          window.alert(err.data.error);
          setIsButtonDisabled(false);
        });
    }
  };

  const handleUnsubscribe = () => {
    if (isSubscribed) {
      setIsButtonDisabled(true);
      unsubscribe(email)
        .then(() => {
          // setIsSubscribed(false);
          dispatch(isUnsubscribedAction())
          setIsButtonDisabled(false);
          setButtonText("Subscribe");
          setEmail("");
          dispatch(removeUserAction(email));
        })
        .catch((err) => {
          console.log(err, err.data);
          window.alert(err.data.error);
          setIsButtonDisabled(false);
        });
    }
  };

  return (
    <section className="app-section app-section--join-us">
      <h2 className="app-title">Join Our Program</h2>
      <p className="app-subtitle app-subtitle--join-us">
        "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
      </p>
      <form className="app-section__input">
        {!isSubscribed && (
          <input
            type="email"
            placeholder="Email"
            className="app-section__input-email"
            value={email}
            onChange={handleEmailChange}
          />
        )}
        <button
          className="app-section__button app-section__button--subscribe"
          type="submit"
          onClick={isSubscribed ? handleUnsubscribe : handleSubscribe}
          disabled={isButtonDisabled}
        >
          {buttonText}
        </button>
      </form>
    </section>
  );
}
