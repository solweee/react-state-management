import React from "react";
import logo_footer from "../assets/your-logo-footer.png";

export function Footer() {
  return (
    <footer className="app-footer">
      <div className="app-footer__logo">
        <img
          src={logo_footer}
          alt="Logo small icon"
          className="app-logo app-logo--small"
        />
        Project
      </div>

      <address className="app-footer__address">
        123 Street,
        <br />
        Anytown, USA 12345
      </address>

      <a className="app-footer__email" href="mailto:hello@website.com">
        hello@website.com
      </a>

      <p className="app-footer__rights">
        &copy; 2021 Project. All rights reserved
      </p>
    </footer>
  );
}
