import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toggleSection, fetchCommunityData } from "../store/actions";

export function Community() {
  const communityData = useSelector((state) => state.visible.communityData);
  const sectionVisible = useSelector((state) => state.visible.sectionVisible);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCommunityData());
  }, [dispatch]);

  return (
    <section className="app-section app-section--community">
      <h2 className="app-title app-title_community">
        Big Community of
        <br />
        People Like You
      </h2>
      <button className="btn_toggle" onClick={() => dispatch(toggleSection())}>
        {sectionVisible ? "Hide Section" : "Show section"}
      </button>
      {sectionVisible && (
        <div>
          <p className="app-subtitle">
            We’re proud of our products, and we’re really excited
            <br />
            when we get feedback from our users.
          </p>
          <ul className="app-section__cards">
            {communityData.map((data) => (
              <li className="card" key={data.id}>
                <img
                  className="card__avatar"
                  src={data.avatar}
                  alt="cat_avatar"
                />
                <p className="card__text">{data.text}</p>
                <h3 className="card__fullname">{`${data.firstName} ${data.lastName}`}</h3>
                <h4 className="card__position">{data.position}</h4>
              </li>
            ))}
          </ul>
        </div>
      )}
    </section>
  );
}
