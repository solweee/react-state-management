/* eslint-disable arrow-body-style */
const sendRequest = async (method, url, data) => {
  const response = await fetch(url, {
    // eslint-disable-next-line object-shorthand
    method: method,
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  });
  if (!response.ok) {
    return response.json().then((errResData) => {
      const error = new Error(`${response.status}: ${response.statusText}`);
      error.data = errResData;
      throw error;
    });
  }
  return await response.json();
};

function subscribe(emailInput) {
  return sendRequest("POST", "http://localhost:3000/subscribe", {
    email: emailInput,
  });
}

function unsubscribe(emailInput) {
  return sendRequest("POST", "http://localhost:3000/unsubscribe", {
    email: emailInput,
  });
}


export { subscribe, unsubscribe, sendRequest };
